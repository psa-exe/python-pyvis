# -*- coding: utf-8 -*-
"""\
===========================
Image manipulation routines
===========================
"""

import numpy as np

def transform(image, matrix, shape, interp='nearest'):
    """Applies a perspective transformation to an image.
    
    Parameters
    ----------
        image : :class:`numpy.ndarray`, required
            Image to be transformed.
        matrix : :class:`numpy.ndarray`, required
            Perspective transformation matrix.
        shape : tuple, required
            Dimensions of resulting image. Only the height and width are 
            considered, depth is copied from source image.
        interp : string, optional
            Interpolation type to use when fetching pixels in source image.
            Possible values: ``'nearest'`` (default), ``'linear'`` and 
            ``'cubic'``.
            
    TODO
        * Additional clipping modes
            
    Returns
    -------
        newimage : :clas:`numpy.ndarray`
            Result of image transformation.
    """
    if len(image.shape) == 3:
        (h, w), d = shape, image.shape[2]
    else:
        (h, w), d = shape, 1
        
    if interp in ['linear', 'cubic']:
        raise NotImplementedError('Interpolation type not implemented: %s' % interp)
    
    # Set of all possible pixel coordinates in world (target) space
    Xw = np.dstack(np.mgrid[0:w, 0:h, 1:2]).reshape((w*h,-1))
    
    # Set of all corresponding pixel coordinates in image (source) space
    Xi = np.dot(matrix, Xw.T).T
    
    # Pesrpective division, clipping and truncating
    Xi[:,0] /= Xi[:,2]
    Xi[:,1] /= Xi[:,2]
    Xi[:,0] = Xi[:,0].clip(0, image.shape[1]-1)
    Xi[:,1] = Xi[:,1].clip(0, image.shape[0]-1)
    Xi = Xi.astype(np.int)
    
    # Creating and populating the target image with the proper source pixels
    newimage = np.zeros((h, w, d), dtype=np.uint8)
    newimage[Xw[:,1], Xw[:,0], :] = image[Xi[:,1], Xi[:,0], :]
    
    return newimage