# -*- coding: utf-8 -*-
"""\
=======================================
Callibration and reconstruction methods
=======================================
"""

import image as img
import numpy as np
import numpy.linalg as npl

def homographyFromPointPairs(pairs, scale=1.0):
    """Computes a homography (a projectivity in 2-dimensional projective space)
    based on four point correspondences.
    
    The homography is computed according to the method described in Example 
    2.12 of the book "Multiple View Geometry in Computer Vision": given 4 point 
    pairs indicating a mapping from world to image space coordinates, a
    homography matrix that maps world points into homogeneous image points is 
    computed. Hence, after transforming world points using the computed matrix,
    one needs to divide the resulting points by the last coordinate to get 
    planar coordinates. Conversely, when applying the inverse matrix to map 
    image points into world points, post-transform division by the last 
    coordinate is needed to obtain planar world coordinates without homogeneous
    ambiguity.
    
    Parameters
    ----------
    pairs : Sequence, required      
        A sequence of four point pairs, where each point is specified as a 
        sequence of planar 2-dimensional coordinates. The first point in each
        pair is regarded as a point in world space (i.e. the source space) and 
        the second one as a point in image space (i.e. the target space).
    scale : number, optional
        Global scale applied to the matrix coefficients, roughly corresponding
        to the 3,3 element of the homography matrix. Should not be zero.
        
    Returns
    -------
    H : numpy.ndarray
        A 3x3 homography matrix that transforms world points into homogeneous
        image points.
    """
    
    pairs = np.array(pairs)

    assert pairs.shape == (4, 2, 2), '4 point pairs are needed to compute a homography, but the given shape was %s' % pairs.shape
    
    worldPoints = pairs[:,0,:]
    imagePoints = pairs[:,1,:]
    
    A = np.zeros((8, 8))
    A[0::2,:] = ([
        [wp[0], wp[1], 1, 0, 0, 0, -wp[0] * ip[0], -wp[1] * ip[0]]
        for wp, ip in zip(worldPoints, imagePoints)
    ])
    A[1::2,:] = ([
        [0, 0, 0, wp[0], wp[1], 1, -wp[0] * ip[1], -wp[1] * ip[1]]
        for wp, ip in zip(worldPoints, imagePoints)
    ])
    
    b = np.hstack((ip[:2] for ip in imagePoints))
    
    H = np.hstack((npl.solve(A, b), [1])).reshape((3, 3))
    
    return H
    
def fourPointsPlanarRectification(origImage, corners, shape):
    """Rectifies an image plane based on four point markings indicating corners
    of a region that is known to be rectangular in Euclidean space.
    
    This algorithm corresponds to the method described in Example 2.12 of the 
    book "Multiple View Geometry in Computer Vision". First, a homography is
    computed by associating the image points in ``corners`` with the corners of
    the world rectangle with dimensions indicated by ``regiondims``, then...
    
    Parameters
    ----------
        origImage : :class:`numpy.ndarray`, required
            The image ro be rectified.
        corners : sequence, required    
            A sequence of image points to be mapped into the reference world 
            rectangles, each represented as a sequence of 2-vectors. Must be 
            specified in the following order: upper left, upper right, bottom 
            right, bottom left.
        shape : tuple, required
            A pair of numbers indicating the dimensions (vert., horiz.) of the 
            world space rectangle (that is, the target image) onto which the 
            plane patch from the source image is mapped.
    """
    h, w = shape
    
    pairs = np.zeros((4, 2, 2))
    pairs[:,0,:] = np.array([[0, 0], [w, 0], [w, h], [0, h]])
    pairs[:,1,:] = corners
    
    H = homographyFromPointPairs(pairs)
    
    return img.transform(origImage, H, shape)