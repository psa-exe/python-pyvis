# -*- coding: utf-8 -*-
"""\
=====
Pyvis
=====

A Python 2.7 library implementing many of the algorithms from the book 
`Multiple View Geometry in Computer Vision`.
"""

__author__ = 'Pedro Asad'
__copyright__ = 'Copyright 2016, UFRJ'
__credits__ = ['Pedro Asad']

__license__ = 'Creative Commons (CC BY 3.0)'
__version__ = '0.0.0.0'
__maintainer__ = 'Pedro Asad'
__email__ = 'pasad@cos.ufrj.br'
__status__ = 'Development'